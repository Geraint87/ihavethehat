﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace IHaveTHeHat.Asp
{
	public class HatHub : Hub
	{
		public void SendMessage(string name, bool iHaveTheHat)
		{
			var message = iHaveTheHat ? "I have the hat!" : "The hat is back";

			Clients.Others.setButtonDisabled(iHaveTheHat);
			Clients.All.broadcastMessage(name, message);
		}
	}
}