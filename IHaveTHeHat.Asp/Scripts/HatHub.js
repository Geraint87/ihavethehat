﻿var hat;

$(function () {
    'use strict';
    // Declare a proxy to reference the hub.
    hat = $.connection.hatHub;

    // Create a functions that the hub can call to broadcast messages.
    hat.client.setButtonDisabled = setButtonDisabled;
    hat.client.broadcastMessage = broadcastMessage;

    // Start the connection.
    $.connection.hub.start().done(function () {
        $('#iHaveTheHat').change(onChange);
    });

    // Get the user name and store it to prepend to messages.
    $('#displayname').val(prompt('Enter your name:', ''));
});

function onChange() {
    'use strict';
    // Call the Send method on the hub.
    hat.server.sendMessage($('#displayname').val(), $(this).prop('checked'));
}

function setButtonDisabled(disable) {
    'use strict';
    // Enable/Disable the button
    var property = disable
        ? 'disable'
        : 'enable';

    $('#iHaveTheHat').bootstrapToggle(property);
}

function broadcastMessage(name, message) {
    'use strict';
    // Html encode display name and message.
    var encodedName = $('<div />').text(name).html();
    var encodedMsg = $('<div />').text(message).html();

    // Add the message to the page.
    $('#discussion').append('<li><strong>' + encodedName
            + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
}
