# README #

### HatStand ###

```
            `;+#'.        
        +@@@#####++`      
    @@@@@@@@######+#          
 :@@@@@@@@@@+#####+#      
,@@@@@@@#'@@@#####++      
+#++'+@@@@@@@######+      
 @@@@@@@@@@@@@#####+      
 :@@@@@@@@@@@@######    
   @@@@@@@@@@@@#####    
   ,@@@@@@@@@@@@####     
    @@@@@@@@@@@@####'+###;
     @@@@@@@@@@@#@''''##++
     @@@@@@@@@@+'''''###++
     +#@@##+++++''''####+#
     ++++++++++++'@###### 
   @@@++++++++'@@######+  
  @@@@@@@@@@@@@@@####@    
  @@@@@@@@@@@@@@@##@      
   '@@@@@@@@@@@#'             

```
A notification tool to let developers know when changes are being published to major projects. When you are putting on your "Publisher's hat", you must come to the HatStand. 
* V1.0.0.0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Further enhancements ###

* Design improvements
* Single sign-on
* Email notifications
* Toasts
* Widget
* WPF client
* Logging and monitoring

### Who do I talk to? ###

* Geraint